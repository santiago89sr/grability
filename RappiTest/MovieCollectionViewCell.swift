//
//  MovieCollectionViewCell.swift
//  RappiTest
//
//  Created by Santiago Rodriguez on 30/06/17.
//  Copyright © 2017 Santiago Rodriguez. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var image: UIImageView!
    
}
