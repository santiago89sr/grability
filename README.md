Clases creadas:

- ViewController.swift: En esta clase se desarrollo la tabla de categorías con el fin de filtrar las películas.
- CategoriesTableViewCell.swift: En esta clase se encuentran cada una de las properties para la celda utilizada en la tabla dentro de la clase viewController.swift
- MoviesListViewController.swift: En esta clase se desarrollo cada una de las consultas para consumir los servicios web, dependiendo de la categoría que se hubiera escogido em el ViewController.swift. También se desarrollo la función de búsqueda dentro de las películas cargadas.
- MoviesCollectionViewCell.swift: Esta clase se encuentra todas las properties que se utilizan dentro de la celda para el collectionview que se encuentra dentro de la clase MoviesListViewController.
- Movie.swift: esta clase se creo con el fin de crear el objeto película y poder utilizar dentro del app.
- MovieViewController.swift: Esta clase se puede observar todos los detalles de cada película, dependiendo de cual ah sido la seleccionado dentro del app.